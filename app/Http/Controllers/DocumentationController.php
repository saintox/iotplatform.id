<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DocumentationController extends Controller
{
    /**
     * Show the documentation of platform.
     *
     * @return \Illuminate\Http\Response
     */
    public function documentation()
    {
        return view('documentation');
    }
}
