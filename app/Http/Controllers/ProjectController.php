<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\API\ApiHelper;
use App\Repos\Repository;
use Illuminate\Http\Request;


class ProjectController extends Controller
{
    use ApiHelper;

    /**
     * @var Repository
     */
    protected $model;
    /**
     * ChannelController constructor.
     *
     * @param Channel $channel
     */
    public function __construct(Project $project)
    {
        $this->model = new Repository( $project );

        // Protect all except reading
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user_id = $request->user()->id;

        return $this->model->with('user')->where('user_id', $user_id)->latest()->paginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // run the validation
        $this->beforeCreate($request);

        return $request->user()->project()
            ->create( $request->only($this->model->getModel()->fillable));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->model->with('device')->findOrFail($id);
    }
    

    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->beforeUpdate($request);

        // validate the channel id belongs to user
        if( ! $request->user()->project()->find($id) ) {
            return $this->errorForbidden('fail');
        }

        if (! $this->model->update($request->only($this->model->getModel()->fillable), $id) ) {
            return $this->errorBadRequest('Unable to update.');
        }

        return $this->model->find($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        // run before delete checks
        if (! $request->user()->project()->find($id)) {
            return $this->errorNotFound('project not found.');
        }

        return $this->model->delete($id) ? $this->noContent() : $this->errorBadRequest();
    }
}
