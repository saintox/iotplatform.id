<?php

namespace App\Http\Controllers;

use App\Models\Device;
use App\Models\User;
use App\Models\Project;
use App\Models\Status;
use App\Repos\Repository;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Validator;

class StatusesManagementController extends Controller
{
    


    public function getDeviceByProjectname($Devicename)
    {
        return Project::with('device')->wherename($projectname)->firstOrFail();
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
    //     $user_id = Auth::id();

    //     //$projects = User::find($user_id)->project->paginates(env('PROJECT_LIST_PAGINATION_SIZE'));

    //     $devices = Device::where('user_id',$user_id)->paginate(env('PROJECT_LIST_PAGINATION_SIZE'));   

    //     return view('devicesmanagement.show-devices', compact('devices'));
    // }

    

    public function form_input($id){
        //$projects = Project::where(['id'=> $id]);
        return view('statusesmanagement.create-status',compact('id'));
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validator = Validator::make($request->all(),
            [
                'nilai'           => 'required',
                'satuan'           => 'required',
                
                
            ],
            [
                'nilai.required'      => trans('statusesmanagement.nilaiNameRequired'),
                'satuan.required'      => trans('statusesmanagement.satuanNameRequired'),                            
            ]
        );

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $id = $request->input('device_id');
                      
        $status = Status::create([
            'nilai'          => $request->input('nilai'),
            'satuan'         => $request->input('satuan'),
            'user_id'        => Auth::id(),
            'device_id'      => $request->input('device_id'),
            
            
            
        ]);

        
        return redirect('devices/'.$id)->with('success', trans('statusesmanagement.createSuccess'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $device = Device::find($id);

        // $user = User::find($id);


        return view('devicesmanagement.show-device')->withDevice($device);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    // public function edit($id)
    // {
    //     $device = Device::findOrFail($id);
        

    //     $data = [
    //         'device'        => $device,
            
    //     ];

    //     return view('devicesmanagement.edit-device')->with($data);
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, $id)
    // {
        
    //     $project = Device::find($id);


    //     $validator = Validator::make($request->all(), [
                
    //             'nilai'            => 'required',
    //             'satuan'        => 'required',
    //         ]);
        

    //     if ($validator->fails()) {
    //         return back()->withErrors($validator)->withInput();
    //     }

        
    //     $device->type = $request->input('type');
    //     $device->location = $request->input('location');

        
    //     $device->save();

    //     return back()->with('success', trans('devicesmanagement.updateSuccess'));
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $currentDevice = user::device();
        $device = Device::findOrFail($id);
        $ipAddress = new CaptureIpTrait();

        if ($device->id != $currentdevice->id) {
            $device->deleted_ip_address = $ipAddress->getClientIp();
            $device->save();
            $device->delete();

            return redirect('devices')->with('success', trans('devicesmanagement.deleteSuccess'));
        }

        return back()->with('error', trans('devicesmanagement.deleteSelfError'));
    }

    /**
     * Method to search the devices.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $searchTerm = $request->input('device_search_box');
        $searchRules = [
            'device_search_box' => 'required|string|max:255',
        ];
        $searchMessages = [
            'device_search_box.required' => 'Search term is required',
            'device_search_box.string'   => 'Search term has invalid characters',
            'device_search_box.max'      => 'Search term has too many characters - 255 allowed',
        ];

        $validator = Validator::make($request->all(), $searchRules, $searchMessages);

        if ($validator->fails()) {
            return response()->json([
                json_encode($validator),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $results = Device::where('id', 'like', $searchTerm.'%')
                            ->orWhere('name', 'like', $searchTerm.'%')
                            ->orWhere('email', 'like', $searchTerm.'%')->get();

        // Attach roles to results
        foreach ($results as $result) {
            $roles = [
                'roles' => $result->roles,
            ];
            $result->push($roles);
        }

        return response()->json([
            json_encode($results),
        ], Response::HTTP_OK);
    }
}
