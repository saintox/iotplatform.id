<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'devicename','project_id','user_id','onoff'
    ];

    protected $casts = [
        'user_id' => 'integer',
        'project_id' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @param bool $forUpdate
     * @return array
     */
    public function getValidationRules($forUpdate = false)
    {
    	$createRule = [
    		'devicename' => 'required|max:30',
    		'project_id' => 'required|integer'
    	];

        $updateRule = [
            'devicename' => 'max:30',
            'onoff' 	 => 'integer'
        ];
        return $forUpdate ? $updateRule : $createRule;
    }	

    public function user()
    {
        return $this->belongsTo(User::class)->select('id','name');
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function status()
    {
        return $this->hasMany(Status::class);
    }
}
