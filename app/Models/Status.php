<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'nilai', 'satuan', 'device_id'];

    protected $casts = [
        'device_id' => 'integer',
        'user_id' => 'integer',
        
    ];

    /**
     * Validation rules
     *
     * @param bool $forUpdate
     * @return array
     */
    public function getValidationRules($forUpdate = false)
    {
        $createRule = [
            'device_id' => 'required|integer',
            'nilai' => 'required',
            'satuan' => 'required'
        ];

        $updateRule = [
            'nilai' => 'required',
            'satuan' => 'required'
        ];

        return $forUpdate ? $updateRule : $createRule;
    }

    public function user()
    {
        return $this->belongsTo(User::class)->select('id', 'name');
    }

    public function device()
    {
        return $this->belongsTo(Device::class);
    }

}
