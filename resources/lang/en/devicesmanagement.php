<?php

return [

    // Validators
    'deviceNameTaken'    => 'Sensor name is taken',
    'deviceNameRequired' => 'Sensor name is required',
    

    // Titles
    'showing-all-devices'     => 'Showing All Sensor',
    'devices-menu-alt'        => 'Show Devices Management Menu',
    'add-new-device'          => 'Add New Sensor',
    'show-deleted-devices'   => 'Show Deleted Device',
    'showing-device'         => 'Showing Device :name',
    'showing-device-title'   => ':name\'s Information',

    // Flash Messages
    'createSuccess'   => 'Successfully created sensor! ',
    'updateSuccess'   => 'Successfully updated device! ',
    'deleteSuccess'   => 'Successfully deleted sensor! ',
    'deleteSelfError' => 'You cannot delete yourself! ',

    // Show device Tab
    'viewProfile'            => 'View Profile',
    'deleteDevice'          => 'Delete Device',
    'devicesBackBtn'        => 'Back to Devices',
    'devicesPanelTitle'     => 'Sensor Information',
    'labelDeviceName'       => 'Sensorname:',
    'labelSensor'           => 'Sensor:',
    'labelStatus'            => 'Status:',
    'labelAccessLevel'       => 'Access',
    'labelPermissions'       => 'Permissions:',
    'labelIpEmail'           => 'Email Signup IP:',
    'labelIpEmail'           => 'Email Signup IP:',
    'labelIpConfirm'         => 'Confirmation IP:',
    'labelIpSocial'          => 'Socialite Signup IP:',
    'labelIpAdmin'           => 'Admin Signup IP:',
    'labelIpUpdate'          => 'Last Update IP:',
    'labelDeletedAt'         => 'Deleted on',
    'labelIpDeleted'         => 'Deleted IP:',
    'devicesDeletedPanelTitle' => 'Deleted Device Information',
    'devicesBackDelBtn'        => 'Back to Deleted Devices',

    'successRestore'    => 'Device successfully restored.',
    'successDestroy'    => 'Device record successfully destroyed.',
    'errorDeviceNotFound' => 'Device not found.',

    'labelDeviceLevel'  => 'Level',
    'labelDeviceLevels' => 'Levels',

    'devices-table' => [
        'caption'   => '{1} :devicescount device total|[2,*] :devicescount total devices',
        'id'        => 'ID',
        'name'      => 'Name',
        'actions'   => 'Actions',
        'updated'   => 'Updated',
    ],

    'buttons' => [
        'add-new'    => '<span class="hidden-xs hidden-sm">New Device</span>',
        'delete'        => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Delete</span><span class="hidden-xs hidden-sm hidden-md"> Device</span>',
        'show'          => '<i class="fa fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Show</span><span class="hidden-xs hidden-sm hidden-md"> Device</span>',
        'back-to-devices' => '<span class="hidden-sm hidden-xs">Back to </span><span class="hidden-xs">Devices</span>',
        'back-to-device'  => 'Back  <span class="hidden-xs">to Device</span>',
        'delete-device'   => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs">Delete</span><span class="hidden-xs"> Device</span>',
    ],

    'tooltips' => [
        'delete'        => 'Delete',
        'show'          => 'Show',
        'add-new'       => 'Add New Device',
        'back-devices'  => 'Back to devices',
        'submit-search' => 'Submit Devices Search',
        'clear-search'  => 'Clear Search Results',
    ],

    'messages' => [
        'deviceNameTaken'          => 'Devicename is taken',
        'deviceNameRequired'       => 'Devicename is required',
        'device-creation-success'  => 'Successfully created device!',
        'update-device-success'    => 'Successfully updated device!',
        'delete-success'           => 'Successfully deleted the device!',
        'cannot-delete-yourself'   => 'You cannot delete yourself!',
    ],

    'show-device' => [
        'id'                => 'Device ID',
        'name'              => 'Devicename',
        'labelAccessLevel'  => '<span class="hidden-xs">Device</span> Access Level|<span class="hidden-xs">Device</span> Access Levels',
    ],

    'search'  => [
        'title'             => 'Showing Search Results',
        'found-footer'      => ' Record(s) found',
        'no-results'        => 'No Results',
        'search-devices-ph'   => 'Search Devices',
    ],

    'modals' => [
        'delete_device_message' => 'Are you sure you want to delete :device?',
        
    ],
];
