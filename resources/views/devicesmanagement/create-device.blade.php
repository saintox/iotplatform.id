@extends('layouts.app3')

@section('template_title')
  Create New Sensor
@endsection

@section('template_fastload_css')
@endsection

@section('content')
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/home') }}" title="Go to Home" class="tip-bottom"><i class="fa fa-home"></i> Home</a> > <a href="{{ url('/projects') }}" title="Go to Create New Projects" class="tip-bottom"><i class="fa fa-database"></i> Projects</a> > <a href="{{ URL::to('projects/' . $id) }}" title="Go to My Devices" class="tip-bottom"><i class="fa fa-laptop"></i> Sensor</a> > <a href="#" class="current"><i class="fa fa-tablet"></i> Add New Sensor</a></div>
  </div>
  <div class="container-fluid">
    <hr>
    @include('partials.form-status')
    <div class="row-fluid" style="margin-left: 200px;">
      <div class="span7">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="fa fa-tablet"></i> </span>
            <h5>Add New Sensor</h5>
          </div>
          <div class="widget-content nopadding">

            {!! Form::open(array('action' => 'DevicesManagementController@store', 'class' => 'form-horizontal')) !!}
            
            <div class="control-group has-feedback row {{ $errors->has('devicename') ? ' has-error ' : '' }}">
                {!! Form::label('devicename', trans('forms.create_device_label_devicename'), array('class' => 'col-md-3 control-label')); !!}
                <div class="controls">
                    <div class="input-group">
                    {!! Form::text('devicename', NULL, array('id' => 'devicename', 'class' => 'form-control', 'placeholder' => trans('forms.create_device_ph_devicename'))) !!}
                        <input type="hidden" name="project_id" value="{{$id}}">
                        <i class="fa fa-fw {{ trans('forms.create_device_icon_devicename') }}" aria-hidden="true"></i>
                    </div>
                    @if ($errors->has('devicename'))
                    <span class="help-block">
                        <strong>{{ $errors->first('devicename') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-actions">           
                {!! Form::button('<i class="fa fa-device-plus" aria-hidden="true"></i>&nbsp;' . trans('forms.create_device_button_text'), array('class' => 'btn btn-success btn-flat margin-bottom-1 pull-right','type' => 'submit', )) !!} 
            </div>                                      
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('footer_scripts')
@endsection