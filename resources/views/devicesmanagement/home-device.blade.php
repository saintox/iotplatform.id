@extends('layouts.app3')

@section('template_title')
    Sensor
@endsection

@section('template_fastload_css')
@endsection

@section('content')
    <div id="content">
        <div id="content-header">
            <div id="breadcrumb"> <a href="{{ url('/home') }}" title="Go to Home" class="tip-bottom"><i class="fa fa-home"></i> Home</a> > <a href="{{ url('/projects') }}" title="Go to Create New Projects" class="tip-bottom"><i class="fa fa-database"></i> Projects</a> > <a href="#" class="current"><i class="fa fa-laptop"></i> Add New Sensor</a></div>
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="span6">
                        <div class="widget-box">
                            <div class="widget-title"> <span class="icon"><i class="fa fa-info"></i></span>
                                <h5>{{ trans('projectsmanagement.projectsPanelTitle') }}</h5>
                            </div>
                            <div class="widget-content nopadding">
                                <ul class="recent-posts">
                                    <li>
                                        <div class="user-thumb"> <img width="150" height="150" alt="User" src="{{ asset('images/backend_images/Cube.jpeg') }}"> </div>
                                        <div class="article-post"> <span class="user-info"><h3> Project Name : {{$project->projectname}} </h3></span> </div>
                                        <p>Manage your application here. You can add/remove devices, show your device detail, etc.</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="widget-box">
                            <div class="widget-title"> <span class="icon"><i class="fa fa-info"></i></span>
                                <h5>{{ trans('projectsmanagement.projectsPanelTitle') }}</h5>
                            </div>
                            <div class="widget-content nopadding">
                                <ul class="recent-posts">
                                    <li>
                                        <div class="article-post"> <span class="user-info"><h5> Type : {{$project->type}} </h5></span> </div>
                                        <div class="article-post"> <span class="user-info"><h5> Location : {{$project->location}} </h5></span> </div>
                                        <div class="article-post"> <span class="user-info"><h5> URL :  iotplatform.id/api/v1/project/{{$project->id}}</h5></span> </div>
                                        <div class="article-post"> <span class="user-info"><h5> Created : {{$project->created_at}} </h5></span> </div>
                                        <div class="article-post"> <span class="user-info"><h5> Last Update : {{$project->updated_at}} </h5></span> </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
        <hr>
            <div class="row">
                <div class="col-sm-6 col-md-12 text-center" style="margin-top: 80px;">
                    <div class="row justify-content-md-center">
                        <div class="col-8 col-md-auto">
                            <h3 class="text-muted">Congratulations!</h3>
                            <h3 class="text-muted">Let's Add Your First Sensor</h3>
                            <br>
                            <a class="nav-link btn btn-primary btn-lg" href="{{url('devices/created') }}/{{$project->id}}">
                                <i class="fa fa-plus"></i>
                                <span class="hidden-sm-down"> Add a Sensor</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection