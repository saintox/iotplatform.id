@extends('layouts.app3')

@if (count($devices) === 0)
    @section('content')

        @include('devicesmanagement.home-device')

    @endsection
    
@else

    @section('template_title')
      Showing Sensor
    @endsection

    @section('template_linked_css')
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
        <style type="text/css" media="screen">
            .devices-table {
                border: 0;
            }
            .devices-table tr td:first-child {
                padding-left: 15px;
            }
            .devices-table tr td:last-child {
                padding-right: 15px;
            }
            .devices-table.table-responsive,
            .devices-table.table-responsive table {
                margin-bottom: 0;
            }

        </style>
    @endsection

    @section('content')
    <div id="content">
        <div id="content-header">
            <div id="breadcrumb"> <a href="{{ url('/home') }}" title="Go to Home" class="tip-bottom"><i class="fa fa-home"></i> Home</a> > <a href="{{ url('/projects') }}" title="Go to Create New Projects" class="tip-bottom"><i class="fa fa-database"></i> Projects</a> > <a href="#" class="current"><i class="fa fa-laptop"></i> My Sensor</a></div>
            <h1> @lang('devicesmanagement.showing-all-devices')</h1>
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="span6">
                        <div class="widget-box">
                            <div class="widget-title"> <span class="icon"><i class="fa fa-info"></i></span>
                                <h5>{{ trans('projectsmanagement.projectsPanelTitle') }}</h5>
                            </div>
                            <div class="widget-content nopadding">
                                <ul class="recent-posts">
                                    <li>
                                        <div class="user-thumb"> <img width="150" height="150" alt="User" src="{{ asset('images/backend_images/Cube.jpeg') }}"> </div>
                                        <div class="article-post"> <span class="user-info"><h3> Project Name : {{$project->projectname}} </h3></span> </div>
                                        <p>Manage your project here. You can add/remove devices, show your device detail, etc.</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="widget-box">
                            <div class="widget-title"> <span class="icon"><i class="fa fa-info"></i></span>
                                <h5>{{ trans('projectsmanagement.projectsPanelTitle') }}</h5>
                            </div>
                            <div class="widget-content nopadding">
                                <ul class="recent-posts">
                                    <li>
                                        <div class="article-post"> <span class="user-info"><h5> Type : {{$project->type}} </h5></span> </div>
                                        <div class="article-post"> <span class="user-info"><h5> Location : {{$project->location}} </h5></span> </div>
                                        <div class="article-post"> <span class="user-info"><h5> URL :  iotplatform.id/api/v1/project/{{$project->id}}</h5></span> </div>
                                        <div class="article-post"> <span class="user-info"><h5> Created : {{$project->created_at}} </h5></span> </div>
                                        <div class="article-post"> <span class="user-info"><h5> Last Update : {{$project->updated_at}} </h5></span> </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                
        <div class="container-fluid">
        <hr>
            @include('partials.form-status')
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"><i class="fa fa-laptop"></i></span>
                            <h5> My Sensor </h5>
                            <div class="pull-right">
                                <a href="{{url('devices/created') }}/{{$project->id}}">
                                    <span class="label label-info"><i class="fa fa-plus"></i> Add a New Sensor</span>
                                </a>
                            </div>
                        </div>
                        <div class="widget-content nopadding">
                            @include('partials.search-devices-form')
                            <table class="table table-bordered data-table">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Sensor name</th>                               
                                            <th colspan="2">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody id="devices_table">
                                        @foreach($devices as $device)
                                            <tr>
                                                <td>{{$device->id}}</td>
                                                <td>{{$device->devicename}}</td>
                                                <td>
                                                    {!! Form::open(array('url' => 'devices/' . $device->id, 'class' => '', 'data-toggle' => 'tooltip', 'title' => 'Delete')) !!}
                                                        {!! Form::hidden('_method', 'DELETE') !!}
                                                        {!! Form::button('<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Delete</span><span class="hidden-xs hidden-sm hidden-md"> sensor</span>', array('class' => 'btn btn-danger btn-sm','type' => 'button', 'style' =>'width: 100%;' ,'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete sensor', 'data-message' => 'Are you sure you want to delete this sensor ?')) !!}
                                                    {!! Form::close() !!}
                                                </td>
                                                <td>
                                                    <a class="btn btn-sm btn-success btn-block" href="{{ URL::to('devices/' .$device->id) }}" data-toggle="tooltip" title="Show">
                                                        <i class="fa fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Show</span><span class="hidden-xs hidden-sm hidden-md"> Sensor</span>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tbody id="search_results"></tbody>
                            </table>
                        </div>
                        <div id="device_count device_pagination" class="form-actions">
                            {{ $devices->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

        @include('modals.modal-delete')

    @endsection

    @section('footer_scripts')

        @include('scripts.delete-modal-script')
        @include('scripts.save-modal-script')
        {{--
            @include('scripts.tooltips')
        --}}

        {{-- @if(config('laraveldevices.enableSearchDevices')) --}}
            @include('scripts.search-devices')
        {{-- @endif --}}

    @endsection

@endif