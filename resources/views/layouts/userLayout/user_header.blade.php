<!--Header-part-->
<div id="header">
  <img src="{{ asset('images/backend_images/logoiot.png') }}" alt="Logo" style="margin-left: 50px; margin-top: 10px" height="150" width="130" />
</div>
<!--close-Header-part--> 

<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
    <li id="profile-messages" class="active"><a title=""><i class="fa fa-user-circle"></i>  <span class="text">Welcome {{ Auth::user()->name }}</span></a>
    </li>
    <li class="">
      @if (Auth::guest())
        <li><a href="{{ route('login') }}">{!! trans('titles.login') !!}</a></li>
        <li><a href="{{ route('register') }}">{!! trans('titles.register') !!}</a></li>
      @else
      <a title="" href="#" data-toggle="modal" data-target="#exampleModal" >
      <i class="fa fa-sign-out"></i> <span class="text">Logout</span></a>
      @endif
    </li>
  </ul>
</div>
<!--close-top-Header-menu-->

<!-- Logout Modal-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave {{ Auth::user()->name }} ?
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </h5>
            </div>

            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                        {!! trans('titles.logout') !!}
                    </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
</div>
