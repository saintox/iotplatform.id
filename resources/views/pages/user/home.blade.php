@extends('layouts.app3')

@section('template_title')
    {{ Auth::user()->name }}'s' Homepage
@endsection

@section('template_fastload_css')
@endsection

@section('content')
<!--main-container-part-->
<div id="content">

    @include('projectsmanagement.home-project')

</div>
                
@endsection