@extends('layouts.app3')

@section('template_title')
  Create New Project
@endsection

@section('template_fastload_css')
@endsection

@section('content')
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ url('/home') }}" title="Go to Home" class="tip-bottom"><i class="fa fa-home"></i> Home</a> > <a href="{{ url('/projects') }}" title="Go to Create New Projects" class="tip-bottom"><i class="fa fa-database"></i> Projects</a> > <a href="#" class="current"><i class="fa fa-folder-open-o"></i> Add New Projects</a></div>
  </div>
  <div class="container-fluid">
    <hr>
    @include('partials.form-status')
    <div class="row-fluid" style="margin-left: 150px;">
      <div class="span9">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="fa fa-cogs"></i> </span>
            <h5>Create New Project</h5>
          </div>
          <div class="widget-content nopadding">

            {!! Form::open(array('action' => 'ProjectsManagementController@store', 'class' => 'form-horizontal')) !!}
          
              <div class="control-group {{ $errors->has('projectname') ? ' has-error ' : '' }}">
              {!! Form::label('projectname', trans('forms.create_project_label_projectname'), array('class' => 'col-md-3 control-label')); !!}
                <div class="controls">
                  <div class="input-group">
                    {!! Form::text('projectname', NULL, array('id' => 'projectname', 'class' => 'form-control', 'placeholder' => trans('forms.create_project_ph_projectname'))) !!} <i class="fa fa-database {{ trans('forms.create_project_icon_projectname') }}" aria-hidden="true"></i>
                  </div>
                  @if ($errors->has('projectname'))
                    <span class="help-block">
                        <strong>{{ $errors->first('projectname') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="control-group {{ $errors->has('type') ? ' has-error ' : '' }}">
              {!! Form::label('type', trans('forms.create_project_label_type'), array('class' => 'col-md-3 control-label')); !!}
                <div class="controls">
                  <div class="input-group">
                    {!! Form::text('type', NULL, array('id' => 'type', 'class' => 'form-control', 'placeholder' => trans('forms.create_project_ph_type'))) !!} <i class="fa fa-tags {{ trans('forms.create_project_icon_email') }}" aria-hidden="true"></i>
                  </div>
                  @if ($errors->has('type'))
                    <span class="help-block">
                        <strong>{{ $errors->first('type') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="control-group {{ $errors->has('location') ? ' has-error ' : '' }}">
              {!! Form::label('location', trans('forms.create_project_label_location'), array('class' => 'col-md-3 control-label')); !!}
                <div class="controls">
                  <div class="input-group">
                    {!! Form::text('location', NULL, array('id' => 'location', 'class' => 'form-control', 'placeholder' => trans('forms.create_project_ph_location'))) !!} <i class="fa fa-map-marker {{ trans('forms.create_project_icon_location') }}" aria-hidden="true"></i>
                  </div>
                  @if ($errors->has('location'))
                    <span class="help-block">
                        <strong>{{ $errors->first('location') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="form-actions">
                {!! Form::button('<i class="fa fa-project-plus" aria-hidden="true"></i>&nbsp;' . trans('forms.create_project_button_text'), array('class' => 'btn btn-success btn-flat margin-bottom-1 pull-right','type' => 'submit', )) !!}
              </div> 
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('footer_scripts')
@endsection
