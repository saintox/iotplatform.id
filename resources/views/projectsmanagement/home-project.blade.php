@extends('layouts.app3')

@if (count($projects) === 0)

    @section('template_title')
        Project
    @endsection

    @section('template_fastload_css')
    @endsection


    @section('content')
    <div id="content">
        <div id="content-header">
            <div id="breadcrumb"><a href="{{ url('/home') }}" title="Go to Home" class="tip-bottom"><i class="fa fa-home"></i> Home</a> > <a href="#" class="current"><i class="fa fa-database"></i> Project</a></div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6 col-md-12 text-center" style="margin-top: 80px;">
                    <div class="row justify-content-md-center">
                        <div class="col-8 col-md-auto">
                            <h3 class="text-muted">Congratulations!</h3>
                            <h3 class="text-muted">Let's Create Your First Project</h3>
                            <br>
                            <a class="nav-link btn btn-primary btn-lg" href="{{ url('projects/create') }}">
                                <i class="fa fa-plus"></i>
                                <span class="hidden-sm-down"> Create a Project</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection
@else

    @section('content')

        @include('projectsmanagement.show-projects')

    @endsection
@endif