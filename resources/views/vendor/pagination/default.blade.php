@if ($paginator->hasPages())
<div id="DataTables_Table_0_paginate" class="dataTables_paginate fg-buttonset ui-buttonset fg-buttonset-multi ui-buttonset-multi paging_full_numbers">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
        <a id="DataTables_Table_0_previous" class="previous fg-button ui-button ui-state-default ui-state-disabled" tabindex="0">Previous</a>
        @else
        <a id="DataTables_Table_0_previous" class="previous fg-button ui-button ui-state-default" tabindex="0" href="{{ $paginator->previousPageUrl() }}" rel="prev">Previous</a>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="disabled"><span>{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                    <a class="fg-button ui-button ui-state-default" tabindex="0"><span>{{ $page }}</span></a>
                    @else
                    <a class="fg-button ui-button ui-state-default" tabindex="0" href="{{ $url }}">{{ $page }}</a>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
        <a id="DataTables_Table_0_next" class="next fg-button ui-button ui-state-default" tabindex="0" href="{{ $paginator->nextPageUrl() }}" rel="next">Next</a>
        @else
        <a id="DataTables_Table_0_next" class="next fg-button ui-button ui-state-default ui-state-disabled" tabindex="0" href="{{ $paginator->nextPageUrl() }}" rel="next">Next</a>
        @endif
</div>
@endif
